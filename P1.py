from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager

import time



# 學號
id="B10807043"

# 宣告成全域變數,才不會閃退
browser = webdriver.Chrome(service=Service(ChromeDriverManager().install()))


# 計算Q2問題
def calculate(num1,num2,operator):
    if operator=='*':
        return num1*num2
    if operator=='+':
        return num1+num2
    if operator=='-':
        return num1-num2
    if operator=='%':
        return num1%num2


def main():

    # 開啟網站
    browser.get("https://aiot.codingmaster.cc/AIoT/Login/")
    # 輸入學號並登入
    browser.find_element(By.NAME,"userID").send_keys("tt"+id)
    browser.find_element(By.XPATH,"/html/body/form/button").click()


    # 跳過前面loading時間,網速太慢需調高
    time.sleep(1)

    # 抓取Result數值,等於200時結束迴圈
    while int(browser.find_element(By.ID,"succesCounter").text)<200:
        # Q2刪除空格
        Q2=browser.find_element(By.ID, "Q2").get_attribute('value').replace(" ","")
        # 填入轉換後的Q1到Q1a
        browser.find_element(By.ID,"Q1a").send_keys(browser.find_element(By.ID, "Q1").get_attribute('value').replace(" ","").replace('|',"tt"+id))
        # Q2前五碼=num1
        # Q2第六碼=operator
        # Q2後五碼=num2
        # calculate(num1,num2,operator)計算後填入答案到Q2a
        browser.find_element(By.ID,"Q2a").send_keys(str(calculate(int(Q2[:5]),int(Q2[6:11]),Q2[5])))
        #按提交按鈕
        browser.find_element(By.ID,"btnSubmit").click()
        #緩衝一下等數值更新
        time.sleep(0.2)


    time.sleep(1)
    #重新整理
    browser.refresh()

if __name__=='__main__':
    main()
